package com.jxg.xgrec.controller;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Title: TestController.java
 *
 * @author jiaxiaogang
 * @date 2018年6月8日
 * @desc
 */
@Repository
public class TestController {
    private static Logger logger = LogManager.getLogger(TestController.class);

    @RequestMapping(value = "/temp/test", method = RequestMethod.GET)
    public void recommendationsV7(HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json; charset=utf-8");
        try {
            response.getWriter().write("abcdefg");
            response.getWriter().flush();
            response.getWriter().close();
        } catch (IOException e) {
            logger.info(e.toString());
        } finally {
            response = null;
        }

    }
}