package com.jxg.xgrec;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XgrecApplication {

    public static void main(String[] args) {
        SpringApplication.run(XgrecApplication.class, args);
    }
}
