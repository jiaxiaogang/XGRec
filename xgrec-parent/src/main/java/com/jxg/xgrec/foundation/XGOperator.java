package com.jxg.xgrec.foundation;


/**
 * 推荐操作入口
 * 1. XGRec不需要理由的学习内容;
 * 2. XGRec不
 *
 */
public class XGOperator {



    /**
     * 提交内容到模型(输入内容,构建推荐模型)
     * @param type:类型,如:作者,文章等内容;
     * @param data:输入的数据;
     */
    public void inputContent(int type,int data) {

    }


    /**
     * 消费内容
     * @param type:
     *            消费类型,如:关注作者,阅读文章,购买商品等;
     *            行为类型,如:点赞,点diss,评论等;
     * @param data
     */
    public void inputConsume(int type,int data) {

    }


    /**
     * 推荐新内容,
     * @param type:内容类型
     * @param data:去重类型(如读过的不要,买过的不要,或者赞过的不要)
     */
    public void outputContent(int type,int data) {

    }


}
